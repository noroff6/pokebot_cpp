#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
#include "restclient-cpp/connection.h"
#include <time.h>
#include <cstdlib>
using json = nlohmann::json;
#define MIN 1
#define MAX 898

int rangeRandomAlg2 (int min, int max){
    srand (time(NULL));
    int n = max - min + 1;
    int remainder = RAND_MAX % n;
    int x;
    do{
        x = rand();
    }while (x >= RAND_MAX - remainder);
    return min + x % n;
}

int main(int, char**) {

RestClient::init();
RestClient::Connection* conn = new RestClient::Connection("http://127.0.0.1:5000/");
RestClient::Connection* pokeapi = new RestClient::Connection("https://pokeapi.co/");
int pokemonId = rangeRandomAlg2(MIN, MAX);
std::string requestUrl = "https://pokeapi.co/api/v2/pokemon/"+std::to_string(pokemonId);
RestClient::Response r = RestClient::get(requestUrl);


json json_body = json::parse(r.body); 

    json data;
    data["name"] = json_body["species"]["name"];
    data["type"] = json_body["types"][0]["type"]["name"];
    data["sprite"] = json_body["sprites"]["front_default"];

    json j = {{"pokemon_id", pokemonId}, {"meta", "pokemon"}, {"data", data}};

    // Convert the data to string
    j["data"] = j["data"].dump();

RestClient::disable();

RestClient::init();

RestClient::Response res =  RestClient::get("http://127.0.0.1:5000/fetch/pokemon/"+std::to_string(pokemonId));
std::cout << res.code << "\n";

if(res.code == 200 )

{
    std::cout << "Already exists, quitting" << "\n";
    return 0;
}

else {
    RestClient::post("http://127.0.0.1:5000/submit", "application/json", j.dump());
}
    

}