cmake_minimum_required(VERSION 3.0.0)
project(pokebot VERSION 0.1.0)

find_package(restclient-cpp REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)

INCLUDE_DIRECTORIES(
    /opt/vcpkg/installed/x64-linux/include
)

include(CTest)
enable_testing()

add_executable(pokebot main.cpp)

TARGET_LINK_LIBRARIES(pokebot
    restclient-cpp
    nlohmann_json
    nlohmann_json::nlohmann_json
)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
