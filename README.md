# Pokebot/ C++

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy for embedded development](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Resources](#resources)
- [Contributors](#contributors)
- [License](#license)

## Background
The program runs a GET request to the Pokemon API. It then runs a GET request to the GPRS (generic python rest server). If the Pokemon has not been caught (is not in the server), it will be added through a POST to the server. If the Pokemon is already in the server the program will shut down.

### Assignment
- [x] Send and receive HTTP messages to the generic python rest server (GPRS)
- [x] Connect to the public Pokemon API and request a Pokemon using an ID from a randomly generated number between 1 and 898
- [x] See if the Pokemon has been caught before, if it has end the program 
- [x] Pokemon information is stored on the GPRS as single JSON object string
- [x] Each time the program executes a new pokemon will be added to the database if it has not been caught
- [x] Cron task scheduler used to run the program every minute
- [x] Farming Pokemons for an hour and more through chron task scheduler

## Implementation
The project was implemented on VS Code. For some farming of the Pokemons we used a Raspberry Pi to act as a server. The cron task scheduler was added to perform a run of the program every minute, and collecting those Pokemon's that has not been caught. The original [Python Generic DB REST Server](https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server) was modified so we could simply do a fetch for the id of the Pokemon, and identify if it had been caught before.

## Install
You will need the following:
- [cmake](https://cmake.org/download/)
- [vcpkg](https://vcpkg.io/en/index.html)
- [restclient-cpp](https://github.com/mrtazz/restclient-cpp)
- [nlohmann/json](https://github.com/nlohmann/json)
- [Python Generic DB REST Server: Addition ](https://gitlab.com/noroff6/python-generic-db-rest-server-addition)
- Clone the repo
<br/>
<br/>
The program was coded using Visual Studio Code.

## Usage
- F1 and CMake: Quick Start
- F1 and CMake: Build
- Run: ./"your chosen project name here"



### Run the Python Generic DB REST Server on a Raspberry Pi
In the
```
main.py
```
file of the [Python Generic DB REST Server: Addition ](https://gitlab.com/noroff6/python-generic-db-rest-server-addition), do the following addition:
```
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
```
This will allow you to access the ip address of the Raspberry Pi within your network. Simply exhange the url for your localhost to the Raspberry Pi's ip address if you want to do GET and POST to the RPI server.

#### Get the IP-address of the Raspberry Pi
In Ubuntu or Powershell/Command prompt
```
ping raspberrypi -4
```


## Resources
- [Pokemon api](https://pokeapi.co/)
- [Chron Task Scheduler](https://www.freecodecamp.org/news/cron-jobs-in-linux/)
- [Python Generic DB REST Server (original)](https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server)

## Contributors
This is a project created by [Helena Barmer](https://gitlab.com/helenabarmer) and [Douglas Johansson](https://gitlab.com/Douglas_Johansson).


## License
[MIT](https://en.wikipedia.org/wiki/MIT_License)